<?php

namespace Trego\Broker\Test;

use PhpAmqpLib\Channel\AMQPChannel;
use Trego\Broker\Requests\Request;

class RequestTest extends BaseTestCase
{
    public function testGetChannel()
    {
        parent::setup();
        $request = new Request($this->repo);
        $request->connect();
        $channel = $request->getChannel();

        $this->assertInstanceOf(AMQPChannel::class, $channel);
    }

    public function testCreateRequest()
    {
        parent::setup();
        $request = new Request($this->repo);

        $this->assertInstanceOf(Request::class, $request);
    }
}
