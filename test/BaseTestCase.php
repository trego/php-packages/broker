<?php

namespace Trego\Broker\Test;

use Orchestra\Testbench\TestCase;
use Trego\Broker\Facades\Broker;
use Trego\Broker\BrokerServiceProvider;

class BaseTestCase extends TestCase
{
    protected function getPackageProviders($app)
    {
        return [
            BrokerServiceProvider::class,
        ];
    }

    protected function getPackageAliases($app)
    {
        return [
            'Broker' => Broker::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $config = $app['config'];

        $config->set('amqp', [
            'host' => 'toad-01.rmq.cloudamqp.com',
            'port' => 5672,
            'username' => 'ycrqsmcr',
            'password' => 'Y9zDaI0N9pvKBS9g7YBtzPeX66slxGM3',
            'vhost' => 'ycrqsmcr',

            'exchange' => 'trego',
            'exchange_type' => 'topic',
            'exchange_passive' => false,
            'exchange_durable' => true,
            'exchange_auto_delete' => false,
            'exchange_internal' => false,
            'exchange_no_wait' => false,
            'exchange_arguments' => [],

            'queue' => '',
            'queue_passive' => false,
            'queue_durable' => true,
            'queue_exclusive' => false,
            'queue_auto_delete' => false,
            'queue_wait' => false,
            'queue_arguments' => [],

            'consumer_tag' => '',
            'consumer_no_local' => false,
            'consumer_no_ack' => false,
            'consumer_exclusive' => false,
            'consumer_no_wait' => false,
            'timeout' => 0,
            'persistent' => false,
        ]);
    }

    public function testPublish()
    {
        Broker::publish('test', 'test');
    }
}
