<?php

namespace Trego\Broker\Test;

use Trego\Broker\Repositories\ConfigRepository;

class ConfigurationTest extends BaseTestCase
{
    public function testGetConfig()
    {
        parent::setup();
        $config = new ConfigRepository($this->repo);

        $this->assertTrue('/' === $config->getConfig('vhost'));
    }

    public function testGetConfigs()
    {
        parent::setup();
        $configs = new ConfigRepository($this->repo);

        $this->assertTrue(is_array($configs->getConfigs()));
    }

    public function testMergeConfig()
    {
        parent::setup();
        $config = new ConfigRepository($this->repo);
        $merge = $config->mergeConfigurations(['routing' => 'test']);

        $this->assertTrue(array_key_exists('routing', $merge));
    }
}
