<?php

namespace Trego\Broker\Test;

use Trego\Broker\Requests\Request;

class ConnectionTest extends BaseTestCase
{
    public function testConnect()
    {
        parent::setup();
        $request = new Request($this->repo);
        $connection = $request->connect();

        $this->assertTrue($connection);
    }
}
