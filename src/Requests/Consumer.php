<?php

namespace Trego\Broker\Requests;

use PhpAmqpLib\Exception\AMQPException;
use PhpAmqpLib\Message\AMQPMessage;

class Consumer extends Request
{
    protected $message_count = 0;

    /**
     * Consume a message.
     *
     * @param [type]   $queue
     * @param \Closure $closure
     */
    public function consume(\Closure $closure)
    {
        try {
            $this->message_count = $this->getMessageCount();

            $object = $this;

            $this->channel->basic_consume(
                $this->queue,
                $this->getConfig('consumer_tag'),
                $this->getConfig('consumer_no_local'),
                $this->getConfig('consumer_no_ack'),
                $this->getConfig('consumer_exclusive'),
                $this->getConfig('consumer_no_wait'),
                function ($message) use ($closure, $object) {
                    $closure($message, $object);
                }
            );

            while (count($this->channel->callbacks)) {
                $this->channel->wait();
            }

            return true;
        } catch (AMQPException $e) {
            throw $e;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Acknowledge message after consume.
     *
     * @param AMQPMessage $message
     */
    public function acknowledge(AMQPMessage $message)
    {
        $message->delivery_info['channel']->basic_ack($message->delivery_info['delivery_tag']);

        if ($message->body === 'quit') {
            $message->delivery_info['channel']->basic_cancel($message->delivery_info['consumer_info']);
        }
    }

    /**
     * Reject a message and requeue if want.
     *
     * @param AMQPMessage $message
     * @param bool        $requeue
     */
    public function reject(AMQPMessage $message, $requeue = false)
    {
        $message->delivery_info['channel']->basic_reject($message->delivery_info['delivery_tag'], $requeue);
    }

    /**
     * Get an information about routing key.
     *
     * @param AMQPMessage $message
     */
    public function getRoutingKey(AMQPMessage $message)
    {
        return $message->delivery_info['routing_key'];
    }

    /**
     * Stop consume a message.
     */
    public function stopWhenProcessed()
    {
        if ($this->message_count <= 0) {
            return;
        }
    }

    /**
     * Get a message.
     *
     * @param AMQPMessage $message
     */
    public function getMessage(AMQPMessage $message)
    {
        return $message->body;
    }
}
