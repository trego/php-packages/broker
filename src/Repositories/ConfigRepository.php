<?php

namespace Trego\Broker\Repositories;

use Illuminate\Config\Repository;

class ConfigRepository
{
    protected $configurations = [];

    public function __construct(Repository $config)
    {
        $this->extractConfigurations($config);
    }

    protected function extractConfigurations(Repository $config)
    {
        $this->configurations = $config->all();
    }

    public function mergeConfigurations(array $configurations)
    {
        $this->configurations = array_merge($this->configurations, $configurations);

        return $this;
    }

    public function getConfigs()
    {
        return $this->configurations;
    }

    public function getConfig($key)
    {
        return array_key_exists($key, $this->configurations) ? $this->configurations[$key] : null;
    }
}
